(* 1st question *)
structure COUNTER :
        sig
           val incr : unit -> unit;
           val decr : unit -> unit;
           val get  : unit -> int;
          end
= struct
    val ctrref = ref 0
    fun  incr () = (ctrref := !ctrref + 1)
    fun  decr () = (ctrref := !ctrref - 1)
    fun  get  () = (!ctrref)
end;

(* 
val result = COUNTER.get ();
print(Int.toString result);
*)

(* 2nd question *)
signature COUNTER =
        sig
           val incr : unit -> unit;
           val decr : unit -> unit;
           val get  : unit -> int;
end

functor MkCounter () :> COUNTER =
   struct
    val ctrref = ref 0
    fun  incr () = (ctrref := !ctrref + 1)
    fun  decr () = (ctrref := !ctrref - 1)
    fun  get  () = (!ctrref)
 end;
(*
structure MyCounter = MkCounter();
val () = MyCounter.incr();
val () = MyCounter.incr();
val () = MyCounter.incr();
val result = MyCounter.get();

val () = print ("Current count: " ^ Int.toString result ^ "\n");
*)


