(*1st question*)
fun foldr f x [] = x
  | foldr f x (y::ys) = f (y,(foldr f x ys))

fun foldl f x [] = x
  | foldl f x (y::ys) = foldl f (f (x,y)) ys

(*2nd question*)
fun add (a,b) = a + b;
fun sum xs = foldl add 0 xs

(*3rd question*)

(*partition function using a helper function seperate and foldr*)
fun partition f xs = let
                        fun seperate (x,(pos,neg)) = if f x then
                                                     (x::pos,neg)
                                                   else
                                                     (pos,x::neg)
                     in
                        foldr seperate ([],[]) xs
                     end

(*map function with foldr*)
fun map f xs = foldr (fn (a,b) => f(a)::b) [] xs

(*reverse function with foldl*)
fun reverse xs = foldl (fn (a,b) => b::a) [] xs

(*nth function*)
datatype 'a Find = LookingFor of int
                 | Found      of 'a

datatype 'a option = SOME of 'a | NONE;



fun nthAux (x, y) = let
                        fun helper (Found v ,u)      = Found v
                          | helper (LookingFor v ,u) = if v = 0 then
                                                         Found u
                                                     else
                                                         LookingFor (v - 1)
                    in
                        foldl helper (LookingFor y) x
                    end

fun nth (x, y) = case (nthAux (x, y)) of
                     LookingFor _ => NONE
                   | Found v      => SOME v
