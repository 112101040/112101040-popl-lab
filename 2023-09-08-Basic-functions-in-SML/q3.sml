fun length []=0                                       
  | length (x::xs) = length (xs) + 1
