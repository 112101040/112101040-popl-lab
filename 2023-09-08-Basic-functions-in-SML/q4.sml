fun reverse [] y = y                                  
  | reverse(x::xs) y = reverse xs (x::y)
fun rev x = reverse x []                               
