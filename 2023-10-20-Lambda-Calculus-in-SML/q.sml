(* question 1 *)

datatype expr = Var of Atom.atom
              | App of expr*expr
              | Abs of Atom.atom*expr

(* question 2 *)

fun pretty e =
  case e of
      Var x        => (Atom.toString x)
    | App (e1, e2) => "(" ^ pretty e1 ^ " " ^ pretty e2 ^ ")"
    | Abs (x, e)   => "λ" ^ (Atom.toString x)  ^ "." ^ pretty e

(* question 3 *)

fun free (Var x)        = AtomSet.singleton(x)
  | free (App (e1, e2)) = AtomSet.union (free e1, free e2)
  | free (Abs (x, e))   = AtomSet.subtract (free e, x)

fun bound (Var x)        = AtomSet.empty
  | bound (App (e1, e2)) = AtomSet.union (bound e1,bound e2)
  | bound (Abs (x, e))   = AtomSet.union (AtomSet.singleton(x), bound e)

(* question 4 *)

fun subst (Var y)        (x, e) = if Atom.compare(x, y) = EQUAL  then e
                                  else (Var y)
  | subst (App (e1, e2)) (x, e) = App (subst e1 (x, e), subst e2 (x, e))
  | subst (Abs (y, e1))  (x, e) = if Atom.compare(x,y) = EQUAL then Abs (y, e1)
                                  else Abs (y, subst e1 (x, e))

(* question 5 *)

fun diag x y = if String.isPrefix x y then y^"n"
               else x^"n"

fun diagA x y = diag x (Atom.toString y)

fun fresh names =Atom.atom (AtomSet.foldl (fn (x, y) => (fn z => z y x) diagA) "" names)
