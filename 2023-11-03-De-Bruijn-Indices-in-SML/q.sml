(*question 1*)
datatype deBruijn = deVar of int                
                | dL of deBruijn                
                | dExp of deBruijn * deBruijn  

datatype expr = VAR of string          
                |Exp of expr * expr    
                |L of string * expr    


(*question 2*)
fun foldl f s []        = s
  | foldl  f s (x::xs) = foldl f (f(x,s)) xs


fun uncurry f(a,b)= f a b

fun help [] [] = [#"a"]
    |help [] (y::ys) = if y = #"a"
                          then [#"b"]
                       else [#"a"]
    |help s []        = s
    |help (x::xs) (y::ys) = if x=y
                                then (x:: (help xs ys))
                            else x::xs

fun diag s1 s2 = implode(help (String.explode(s2)) (String.explode(s1)))


fun fresh (sets:string list) = foldl (uncurry(diag)) "" sets

fun deBru_to_FO (dL e ) (var : string list) = L(fresh var , deBru_to_FO e ((fresh var)::var))
    |deBru_to_FO(dExp (e1,e2)) (var:string list) = Exp(deBru_to_FO e1 var, deBru_to_FO e2 var)
    |deBru_to_FO (deVar x) (var:string list) = VAR(List.nth(var,(x-1)))
