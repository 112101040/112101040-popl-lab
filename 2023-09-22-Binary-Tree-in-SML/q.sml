(*1st question*)
datatype 'a tree = Null
       | Node of 'a tree * 'a * 'a tree

(*2nd question*)
fun treemap f (Null) = Null                      (*('a -> 'b) -> 'a tree -> 'b tree*)
  | treemap f (Node(left,root,right))=Node(treemap f (left),f (root),treemap f(right))

(*3rd question*)
fun preorder (Null) = []                         (*'a tree -> 'a list*)
  | preorder (Node(left,root,right)) = [root] @ preorder (left) @ preorder (right)

fun inorder (Null) = []                          (*'a tree -> 'a list*)
  | inorder (Node(left,root,right)) = inorder (left) @ [root] @ inorder (right)

fun postorder (Null) = []                        (*'a tree -> 'a list*)
  | postorder (Node(left,root,right)) = postorder (left) @ postorder (right) @ [root]
 
(*4th question*)

(* fold = fn : ('a * 'b -> 'b) -> 'b -> 'a tree -> 'b*)
fun fold f s t= 
  (case t
    of  Empty => s
      | Node (x , lt, rt) => f(x, fold f s lt, fold f s rt));

(*5th question*)
  
(*in_order' = fn: 'a tree -> 'a list*)
fun in_order' t = fold (fn(x,left_r,right_r) => left_r@[x]@right_r) [] t;

(*post_order' = fn: 'a tree -> 'a list*)
fun pre_order' t = fold (fn(x,left_r,right_r) => [x]@left_r@right_r) [] t;

(*post_order' = fn: 'a tree -> 'a list*)
fun post_order' t = fold (fn(x,left_r,right_r) => left_r@right_r@[x]) [] t;

(*6th question*)
fun rotate_clk_wise (Null) = (Null)
  | rotate_clk_wise (Node(Null,a,t3)) = (Node(Null,a,t3))
  | rotate_clk_wise (Node(Node(t1,b,t2),a,t3)) = (Node(t1,b,Node(t2,a,t3)))
