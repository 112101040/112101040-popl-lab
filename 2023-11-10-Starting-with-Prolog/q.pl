man(sachin).
man(kiran).
man(ravi).
woman(vinny).
woman(lisa).
woman(rani).

parent(sachin,kiran).
parent(rani,kiran).
parent(kiran,vinny).
parent(kiran,ravi).
parent(lisa,vinny).
parent(lisa,ravi).

father(X,Y) :- man(X),parent(X,Y).
mother(X,Y) :- woman(X),parent(X,Y).

grandfather(X,Z) :- father(X,Y),parent(Y,Z).
grandmother(X,Z) :- mother(X,Y),parent(Y,Z).

sibling(X,Y) :- parent(Z,X),parent(Z,Y),X \= Y.

brother(X,Y) :- man(X) , sibling(X,Y).
sister(X,Y) :- woman(X) , sibling(X,Y).
